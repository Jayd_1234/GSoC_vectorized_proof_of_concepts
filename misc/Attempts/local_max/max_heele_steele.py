
# coding: utf-8

# In[1]:


import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
from pycuda.compiler import *
import pycuda.driver as cuda
import timeit


# In[2]:


val = 30000*100
AVENUMJETS = np.array([1,2,5,10,20,50,100,200,500,1000])
NUMEVENTS = val//AVENUMJETS

gpu_time = np.empty_like(AVENUMJETS)
cpu_time = np.empty_like(gpu_time)

for i in range(len(AVENUMJETS)):

    numjets = np.random.poisson(AVENUMJETS[i], NUMEVENTS[i])


    stops = np.cumsum(numjets, dtype=np.int)
    starts = np.zeros_like(stops)
    starts[1:] = stops[:-1]
    offsets = np.zeros(len(stops)+1, dtype=np.int)
    offsets[1:] = stops


    # In[3]:


    data = np.random.randint(low=0, high=10, size=stops[-1]).astype(np.int)


    # In[4]:


    parents = np.empty(stops[-1], dtype=np.int)
    for i in range(len(offsets)-1):
        parents[offsets[i]:offsets[i+1]] = i


    # In[5]:


    mod = DynamicSourceModule('''

    /* Not what we are using. Kept for referance to first working implementation.

    __global__ void heele_max(int* arr,int* offsets,int* parents, int* num_particles)
    {
        unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;
        if (tid > num_particles[0])
            return;
        
        for (int d=0; d<log2(double(num_particles[0])); d++)
        {
            if (tid >=pow(2.0,double(d)) && parents[tid]==parents[tid-int(pow(2.0, double(d)))])
            {
                arr[tid] = max(arr[tid], arr[tid-int(pow(2.0, double(d)))]);
            }
            __syncthreads();
        }
    }

    */

    __global__ void heele_max2(int* arr,int* offsets,int* parents, int* num_particles)
    {
        unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;
        if (tid > num_particles[0])
            return;
        
        for (int d=1; d<num_particles[0]; d*=2)
        {
            if (tid >=d && parents[tid]==parents[tid-d])
            {
                arr[tid] = max(arr[tid], arr[tid-d]);
            }
            __syncthreads();
        }
    }

    __global__ void max_kern(int* arr, int start, int stop)
    {
        unsigned int tid = threadIdx.x + blockDim.x*blockIdx.x;
        if (tid > stop-start)
            return;
        
        for (int d=(stop-start)/2; d>0; d>>2)
        {
            if (tid >= d)
            {
                arr[tid] = max(arr[tid], arr[tid-d]);
            }
            __syncthreads();
        }
        
    }
    __global__ void heele_max3(int* arr,int* offsets,int* parents, int* num_particles)
    {
        unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;
        if (tid > num_particles[0])
            return;
        
        dim3 numthreads = (512,1,1);
        dim3 numblocks = (int((offsets[tid+1]-offsets[tid])/numthreads.x),1,1);
        max_kern<<<numblocks,numthreads>>>(arr, offsets[tid], offsets[tid+1]);
        cudaDeviceSynchronize();
        __syncthreads();
    }
    ''',cuda_libdir="C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v9.1/lib/x64")


    # In[6]:


    # func1 = mod.get_function('heele_max')
    func2 = mod.get_function('heele_max2')
    func3 = mod.get_function('heele_max3')


    # In[7]:


    len_arr = gpuarray.to_gpu(np.array([NUMEVENTS[i]], dtype=np.int32))
    gpu_data = gpuarray.to_gpu(data)
    gpu_parents = gpuarray.to_gpu(parents)
    gpu_offsets = gpuarray.to_gpu(offsets)
    numthreads = 512
    numblocks = int(np.ceil(NUMEVENTS[i]/numthreads))


    # In[8]:


    #func2(gpu_data, gpu_offsets, gpu_parents, len_arr, block=(numthreads,1,1), grid=(numblocks,1))


    # In[9]:
    start_time = cuda.Event()
    stop_time = cuda.Event()

    start_time.record()

    func3(gpu_data, gpu_offsets, gpu_parents, len_arr, block=(numthreads,1,1), grid=(numblocks,1))
    stop_time.record()
    stop_time.synchronize()

    gpu_time[i] = start_time.time_till(stop_time)
    # In[10]:




    # In[ ]:
    #max_arr


    # In[ ]:


    reduce_at_offsets = np.zeros_like(offsets)
    reduce_at_offsets[1:] = stops
    reduce_at_offsets[-1] = reduce_at_offsets[-1]-1


    # In[ ]:

    cpu_time[i] = timeit.timeit("np_max = np.maximum.reduceat(data, reduce_at_offsets)", globals=globals(), number=10)*1000


np.save("max_heele_gpu.npz", gpu_time)
np.save("max_heele_cpu.npz", cpu_time)