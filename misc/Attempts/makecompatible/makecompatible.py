
# coding: utf-8

# In[9]:


import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
from pycuda.compiler import SourceModule
import pycuda.driver as cuda
import numba
import timeit


# In[10]:





# In[11]:


val = 10000*100
AVENUMJETS = np.array([1,2,5,10,20,50,100,200,500])
NUMEVENTS = val//AVENUMJETS

gpu_time = np.empty_like(AVENUMJETS)
cpu_time = np.empty_like(gpu_time)

for i in range(len(AVENUMJETS)):

    numjets = np.random.poisson(AVENUMJETS[i], NUMEVENTS[i])
    stops = np.cumsum(numjets, dtype=np.int32)
    offsets = np.zeros(len(numjets)+1, dtype=np.int32)
    offsets[1:] = stops

    data1 = np.random.randn(offsets[-1]).astype(np.float32)
    data2 = np.random.randn()


    # In[12]:
    def makecompatible_numpy(data, parents):
        good = (parents >= 0)
        content = np.empty(len(parents), dtype=data.dtype)
        if (len(data.shape)==0):
            content[good] = data
        else:
            content[good] = data[parents[good]]
    
        return content

    @numba.jit()
    def parents(offsets, NUMEVENTS):
        out = np.empty(offsets[-1], dtype=np.int32)
        for i in range(NUMEVENTS):
            out[offsets[i]:offsets[i+1]] = i
        
        return out


    # In[13]:


    mod = SourceModule('''
    __global__ void makecompatible(float* data,int* parents,float* out,int* lenparents,int* lendata)
    {
        unsigned int idx = threadIdx.x + blockIdx.x*blockDim.x;
        
        if (idx >= lenparents[0])
            return;
        
        if (parents[idx] <0)
            return;
        
        out[idx] = (lendata[0] == 1) ? data[0] : data[parents[idx]];
    }
    ''')


    # In[14]:


    parents_arr = parents(offsets, NUMEVENTS[i])


    # In[15]:
    s = timeit.default_timer()
    numpy_res = makecompatible_numpy(data1, parents_arr)
    st = timeit.default_timer()
    cpu_time[i] = (st-s)*1000 
    


    # In[16]:


    makecompatible_gpu = mod.get_function("makecompatible")


    # In[17]:


    gpu_parents = gpuarray.to_gpu_async(parents_arr)
    gpu_data = gpuarray.to_gpu_async(data1)
    gpu_out = gpuarray.empty(len(parents_arr), dtype=np.float32)
    gpu_lendata = gpuarray.to_gpu(np.array([len(data1)], dtype=np.int32))
    gpu_lenparents = gpuarray.to_gpu(np.array([len(parents_arr)], dtype=np.int32))

    nthreads = 512
    nblocks = int(np.ceil(len(parents_arr)/512))

    start_time = cuda.Event()
    stop_time = cuda.Event()

    start_time.record()
    makecompatible_gpu(gpu_data,gpu_parents,gpu_out,gpu_lenparents,gpu_lendata,block=(nthreads,1,1),grid=(nblocks,1))
    stop_time.record()
    stop_time.synchronize()

    gpu_time[i] = start_time.time_till(stop_time)

np.save("makecompatible_gpu.npz", gpu_time)
np.save("makecompatible_cpu.npz", cpu_time)