
# coding: utf-8

# # Vectorization of offsets from parents
# 
# This will change the two unvectorized parts in original awkward array version to make it fully vectorized. 
# 
# Here, the data has been generated as usual, and we use the parents algorithm to generate the parents from the starts and stops. Finally, from parents, we again derive the offsets and compare the two versions for proof.

# In[1]:


import numpy 
import numba
import pycuda.autoinit
import pycuda.driver as cuda
from pycuda.compiler import *
import pycuda.gpuarray as gpuarray
import timeit

np = numpy
# In[2]:


val = 30000*100
AVENUMJETS = np.array([1,2,5,10,20,50,100,150,200,270,360,500,740,1000])
NUMEVENTS = val//AVENUMJETS

gpu_time = np.empty_like(AVENUMJETS)
cpu_time = np.empty_like(gpu_time)

for i in range(len(AVENUMJETS)):

    numjets = np.random.poisson(AVENUMJETS[i], NUMEVENTS[i])
    jets_stops = numpy.cumsum(numjets, dtype=numpy.int32)                                      # Stops array
    jets_starts = numpy.zeros_like(jets_stops)                              # Starts array
    jets_starts[1:] = jets_stops[:-1]


    # In[3]:


    NUMPARTICLES = jets_stops[-1]


    # In[4]:


    parents = numpy.empty(jets_stops[-1], dtype=numpy.int32)
    # Sequential evaluation
    @numba.jit()
    def parent(starts, stops, pointers):
        for i in range(len(starts)):
            pointers[starts[i]:stops[i]] = i
    parent(jets_starts, jets_stops, parents)


    # In[5]:


    # Offsets from parents: original awkward array version

    def fromparents(parents, content, starts, counts):
            if len(parents) != len(content):
                raise ValueError("parents array must have the same length as content")

            tmp = numpy.nonzero(parents[1:] != parents[:-1])[0] + 1

            changes = numpy.empty(len(tmp) + 2, dtype=numpy.int)
            changes[0] = 0
            changes[-1] = len(parents)
            changes[1:-1] = tmp

            length = len(starts)

            where = parents[changes[:-1]]
            real = (where >= 0)

            starts[where[real]] = (changes[:-1])[real]
            counts[where[real]] = (changes[1:] - changes[:-1])[real]

            return [starts, counts]

    # In[6]:


    content = numpy.random.randn(jets_stops[-1])
    max_val = parents.max()
    starts = numpy.zeros(max_val+1, dtype=numpy.int32)
    counts = numpy.zeros_like(starts)

    cpu_time[i] = timeit.timeit("starts_awk, counts_awk = fromparents(parents, content, starts, counts)", globals=globals(), number=100)*1000


    # ### Parts that might be unvectorized 
    # 
    # 1. `numpy.nonzero`
    # 2. `parents.max()`
    # 
    # **Solutions**
    # 
    # 1. `numpy.nonzero()` can be replaced with a combination of `numpy.where` with a boolean mask.
    # 2. `numpy.max()` is s sequential evaluation ( atleast I think, as the source is all over the place ). A simpler log() reduction can be applied to calculate this.

    # In[7]:


    # Vectorized max()
    # Based on parallel reduction in bisection search spirit.

    offsets = numpy.zeros(max_val+1, dtype=numpy.int32)

    mod = SourceModule('''
    __global__ void offsets_from_parents(int* parents,int* offsets, int* num_parents)
    {
        // Thread IDs
        unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;

        // Check for corner case, make sure we don't exceed the boundary.
        if (idx >= num_parents[0] - 1)
            return;
        
        // Store the values necessary as local varsiables to reduce data R/W
        int prev_val = parents[idx];
        int next_val = parents[idx + 1];

        // Check for the condition and write the results
        if (prev_val != next_val)
        {
            offsets[next_val] = idx+1;
        }
    }
    ''', options=['-O3'])

    offsets_func = mod.get_function('offsets_from_parents')

    gpu_parents = gpuarray.to_gpu(parents)
    gpu_offsets = gpuarray.to_gpu(offsets)
    gpu_numparents = gpuarray.to_gpu(numpy.array([jets_stops[-1]]).astype(numpy.int32))

    numthreads = 512
    numblocks = int(numpy.ceil(jets_stops[-1]/numthreads))

    starts_time = cuda.Event()
    stop_time = cuda.Event()
    starts_time.record()
    offsets_func(gpu_parents,gpu_offsets,gpu_numparents, block=(numthreads,1,1), grid=(numblocks,1))
    stop_time.record()
    stop_time.synchronize()

    gpu_time[i] = starts_time.time_till(stop_time)

np.save("offsets_gpu.npz", gpu_time)
np.save("offsets_cpu.npz", cpu_time)
