
# coding: utf-8

# In[1]:


import numpy
import pycuda.autoinit
import pycuda.driver as cuda
from pycuda.compiler import *
import pycuda.gpuarray as gpuarray
import pycuda
import numba
import time
import timeit

# ### Python version ( Thanks Jim!)
# 
# It is based on binary search as used in bisect.

# In[2]:


# Pyhton version
'''
def vectorized_search(offsets, content):
    print("on CUDA, this would be %d threads for as many particles" % len(content))
    print("expected convergence in %g steps" % numpy.log2(len(offsets) - 1))

    index = numpy.arange(len(content), dtype=int)                     # threadIdx.x on CUDA
    below = numpy.zeros(len(content), dtype=int)                      # just below = 0 on CUDA
    above = numpy.ones(len(content), dtype=int) * (len(offsets) - 1)  # same for above

    step = 0   # only used for print-outs
    while True:
        middle = (below + above) // 2

        step += 1
        print("step %d: try parents = %s" % (step, str(middle)))

        change_below = offsets[middle + 1] <= index                   # which "belows" must we change?
        change_above = offsets[middle] > index                        # which "aboves"?

        if not numpy.bitwise_or(change_below, change_above).any():    # neither? great! we're done!
            break
        else:
            below = numpy.where(change_below, middle + 1, below)      # vectorized "if" statement
            above = numpy.where(change_above, middle - 1, above)      # this is the only branch

    print("done!")
    return middle


# In[11]:
'''

np = numpy

val = 30000*100
AVENUMJETS = np.array([1,2,5,10,20,50,100,150,200,270,360,500,740,1000])
NUMEVENTS = val//AVENUMJETS

gpu_time = np.empty_like(AVENUMJETS)
cpu_time = np.empty_like(gpu_time)

for i in range(len(AVENUMJETS)):
    numjets = np.random.poisson(AVENUMJETS[i], NUMEVENTS[i])
    stops = numpy.cumsum(numjets, dtype=numpy.int32)
    starts = numpy.zeros_like(stops)
    starts[1:] = stops[:-1]
    offsets = numpy.zeros(NUMEVENTS[i]+1, dtype=numpy.int32)
    offsets[1:] = stops
    offsets[-1] = offsets[-1]+1

    counts = stops-starts


    # In[12]:


    #vectorized_search(offsets,content)


    # ### (1) CUDA Loop version
    # 
    # There is an assumption that `index` is within `len(content)`, but it's easy to evade that, as done many times earlier. 
    # 
    # A similar thing is done in block and grid size determination.

    # In[13]:


    mod = SourceModule('''
    #include <stdlib.h>
    #include <thrust/reduce.h>
    #include <thrust/execution_policy.h>
    #include <thrust/sequence.h>
    #include <thrust/transform.h>

    extern "C" {
    __global__ void vectorized_search(int* offsets, int* middle,int* len_content,int* below,int* above)
    {
        int index = blockIdx.x*blockDim.x + threadIdx.x;
        int tid = threadIdx.x;

        if ( index >= len_content[0])
            return;
        
        
        while (1)
        {
            middle[index] = int((below[index] + above[index])/2);
            if (offsets[middle[index]+1]<=index || offsets[middle[index]]>index)
            {
                below[index] = (offsets[middle[index]+1]<=index)? middle[index]+1 :below[index];
                above[index] = (offsets[middle[index]]>index) ? middle[index]-1: above[index];
            }
            else
                break;
        }
        

    }

    __global__ void thrust_parents(int* starts,int* stops, int* parents,int* numevents)
    {
        unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
        if (idx > numevents[0])
            return;
        thrust::fill(thrust::device, &parents[starts[idx]], &parents[stops[idx]],idx);
    }

    __global__ void vectorized_search2(int* offsets, int* middle,int* len_content,int* below,int* above)
    {
        int index = blockIdx.x*blockDim.x + threadIdx.x;
        int tid = threadIdx.x;

        if ( index >= len_content[0])
            return;
        
        __shared__ int soffsets[512], smiddle[512], sbelow[512], sabove[512];

            
            //soffsets[tid] = offsets[blockIdx.x];
            smiddle[tid] = middle[index];
            sbelow[tid] = below[index];
            sabove[tid] = above[index];
        
        __syncthreads();

        while (1)
        {
            smiddle[tid] = int((sbelow[tid] + sabove[tid])/2);
            if (offsets[smiddle[tid]+1]<=index || offsets[smiddle[tid]]>index)
            {
                sbelow[tid] = (offsets[smiddle[tid]+1]<=index)? smiddle[tid]+1 :sbelow[tid];
                sabove[tid] = (offsets[smiddle[tid]]>index) ? smiddle[tid]-1: sabove[tid];
            }
            else
                break;
        }
        middle[index] = smiddle[tid];
        __syncthreads();
    }

    __global__ void parents2(int* starts,int* counts, int* parents,int* numevents)
    {
        unsigned int idx = threadIdx.x + blockIdx.x*blockDim.x;

        if (idx>=numevents[0])
            return;
        
        const int val = starts[idx];
        for (int i=0; i<counts[idx]; i++)
        {
            parents[val+i] = idx;
        }
    }
    __global__ void parents3(int* starts,int* stops, int* parents,int* numevents)
    {
        unsigned int idx = threadIdx.x + blockIdx.x*blockDim.x;

        if (idx>=numevents[0])
            return;
        
        const int val = starts[idx];
        for (int i=val; i<stops[idx]; i++)
        {
            parents[i] = idx;
        }
    }
    }

    ''',no_extern_c=True)


    # In[6]:
    @numba.jit(cache=False)
    def numpy_parents(parents, starts, stops, numevents):
        for i in range(numevents):
            parents[starts[i]: stops[i]] = i

    func = mod.get_function("vectorized_search2")
    func2 = mod.get_function("parents2")
    func3 = mod.get_function("parents3")
    func4 = mod.get_function('thrust_parents')

    # In[7]:

    g_offsets = gpuarray.to_gpu(offsets)
    g_len_content = gpuarray.to_gpu(numpy.array([stops[-1]], dtype=numpy.int32))
    below = gpuarray.zeros(stops[-1], dtype=numpy.int32)
    above = gpuarray.zeros(stops[-1], dtype=numpy.int32) + (len(offsets)-1)


    gpu_starts = gpuarray.to_gpu(starts)
    gpu_counts = gpuarray.to_gpu(counts)
    gpu_parents = gpuarray.zeros(stops[-1], dtype=numpy.int32)
    gpu_numevents = gpuarray.to_gpu(numpy.array([len(starts)], dtype=numpy.int32))
    gpu_stops = gpuarray.to_gpu(stops)

    parents = numpy.empty(stops[-1], dtype=numpy.int32)
    numthreads = 512
    numblocks = int(numpy.ceil(NUMEVENTS[i]/numthreads))


    # In[8]:


    middle = gpuarray.empty(stops[-1], dtype=numpy.int32)


    # In[9]:
    '''
    start_time = cuda.Event()
    stop_time = cuda.Event()
    start_time.record()
    #func(g_offsets,middle, g_len_content, below, above, block=(numthreads,1,1), grid=(numblocks,1))
    func2(gpu_starts,gpu_counts,gpu_parents,gpu_numevents,block=(numthreads,1,1), grid = (numblocks,1))
    stop_time.record()
    stop_time.synchronize()
    '''

    start_time = cuda.Event()
    stop_time = cuda.Event()
    start_time.record()
    #func3(gpu_starts,gpu_stops,gpu_parents,gpu_numevents,block=(numthreads,1,1), grid = (numblocks,1))
    func4(gpu_starts,gpu_stops,gpu_parents,gpu_numevents,block=(numthreads,1,1), grid = (numblocks,1))
    #func(g_offsets,middle,g_len_content,below, above, block=(numthreads,1,1), grid = (numblocks,1))
    stop_time.record()
    stop_time.synchronize() 
    gpu_time[i] = start_time.time_till(stop_time)
    # In[10]:

    s = timeit.default_timer()
    numpy_parents(parents,starts,stops,NUMEVENTS[i])
    st = timeit.default_timer()
    cpu_time[i] = (st-s)*1000 
    # Print the result

    #print(middle)


    # ### (2) Python loop with gpuarrays
    # 
    # The loop here is in python. However, the arrays are gpuarray instances.
    # This is not very useful, as pycuda doesn't have support for array indexing, and implementing it will result in the same source module as above. 
    # Currently doing with a mix of elementwise and reduction kernels

    # In[11]:
np.save("parents_gpu.npz", gpu_time)
np.save("parents_cpu.npz", cpu_time)