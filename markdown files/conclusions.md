## Conclusions and Future Work

#### Conclusions
From this project, and the algorithms developed, we can conclude the following:

- Algorithms, that can be vectorized give much higher runtime speed compared to their un-vectorized counterparts.

- Fully vectorized algorithms like `argproduct`, are blazingly fast in GPU, compared to it's sequential part.

- Some algorithms require multiple steps to reduce in a vectorized way. Such algorithms ( like `non-zero` or `local reduction`) are likely to perform similar to or slightly better than their CPU parts. 

- The timings prove that vectorization allows efficient use of computing resources, without unnecessary financial burden of upgrading computing resources.

#### Future Work

The major works that we would like to undertake in the future would be:

-  Provide a logical foundation to prove ( or disprove) vectorizability of a sequential code.

- Assimilate a set of important algorithms, and produce a vectorized algorithms library. The CPU algorithms library is already under heavy development by the name of [awkward-array](https://github.com/scikit-hep/awkward-array). 

- Complete implementation of more algorithms on GPU, and assimilate them to provide a GPU accelerated version of [awkward-array](https://github.com/scikit-hep/awkward-array).

