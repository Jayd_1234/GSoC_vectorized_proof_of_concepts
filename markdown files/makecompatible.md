## Makecompatible 

__*CODE:*__ Available [here](https://gitlab.com/Jayd_1234/GSoC_vectorized_proof_of_concepts/blob/master/misc/Attempts/makecompatible/makecompatible.ipynb)
### Introduction

`Makecompatible`  broadcasts a scalar data to multi-event data, or assign a data array to a given array.

In many cases, we would like to initialize an array with a particular scalar value, or to fast copy an array to another, keeping the arrays compatible in first . In such cases, the `makecompatible` function will be useful. 

The working of the algorithm will be explained now.

### Sequential Implementation

Let us consider a initial array `parents`, and a data array `data`, from which we would like to make a compatible array. 

To accomplish this, we consider the two cases:

- `data` is a scalar. In this case, we need to broadcast it across the length of `parents`.

- `data` is a multi-dimensional array. In this case, we need to broadcast it across the first axis.

The sequential code implementing the same is:

```python
import numpy as np

def makecompatible(data, parents):
    content = np.empty(len(parents), dtype=data.dtype)
    if (len(data.shape)==0):
        for i in range(len(parents)):
            content[parents[i]] = data
    else:
        for i in range(len(parents)):
            content[parents[i]] = data[parents[i]]

    return content
```

### Vectorized Implementation

This particular algorithm is quite easy to vectorize. This is because the loop doesn't have any carried dependency. We just need to check for the length of `data`. This makes it particularly suitable for the GPU. 

#### GPU Implementation

The CUDA kernel implementing the idea is given below:

```cpp
__global__ void makecompatible(float* data,int* parents,float* out,int* lenparents,int* lendata)
{
    unsigned int idx = threadIdx.x + blockIdx.x*blockDim.x;
    
    // Check for illegal access
    if (idx >= lenparents[0])
        return;

    // Check for illegal data in parents    
    if (parents[idx] <0)
        return;

    // Assign the value according to shape of data.
    out[idx] = (lendata[0] == 1) ? data[0] : data[parents[idx]];
}
```

The code is straightforward enough. 

1. First, we check if any thread is going beyond array bounds. 

2. Second, we check if any illegal data is present in `parents`, and filter them out. 

3. Finally, assign the data. If the shape of data is 1, then assign data to all the values in output array `out`, else assign `out` to `data[parents[idx]]`.

#### CPU Implementation

The CPU code was implemented in `numpy`. This is because  the code involves a set of vectorized implementations, which `numpy` is great at.

The implementation is given below:

```python
def makecompatible_numpy(data, parents):
    good = (parents >= 0)
    content = np.empty(len(parents), dtype=data.dtype)
    if (len(data.shape)==0):
        content[good] = data
    else:
        content[good] = data[parents[good]]
    
    return content
```



### Performance 

#### Execution environment

The codes were executed on an Amazon AWS G2 instance, which had the following specifications:

__*CPU*__
The CPU in the instance is Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz. It has 2 physical cores, and 4 logical cores, and supports SIMD instructions like AVX and SSE.

__*RAM:*__

The instance had a RAM size of 64 GB.
__*GPU:*__ 

The GPU instance was loaded with a Nvidia Tesla K80 card, which is of model number GK210GL.  It's specifications can be checked [here](https://www.nvidia.com/en-us/data-center/tesla-k80/). 

#### Timings

For timing the results, the time taken for GPU is derived using PyCUDA's `driver.Event()` module. It waits until the device has synchronized, which indicates the end of the kernel call. 

The CPU code was timed with OpenMP's `omp_get_wtime()` for C++ code, and `timeit.timeit()` for numpy version. The best of the two was kept as CPU time taken.

The relative timings were plotted with `matplotlib`. It is given below:

![Timings plot](images/makecompatible_time.png)

The plots are between wall clock time taken in milliseconds, and the Average number of particles.

It can be seen that The CPU time intercept is much higher than GPU, which means that as the data size gets larger, GPU will outperform CPU almost always.