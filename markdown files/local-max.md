## Local Reduction

__*CODE:*__ Available [here](https://gitlab.com/Jayd_1234/GSoC_vectorized_proof_of_concepts/blob/master/misc/Attempts/local_max/max_heele_steele.py)
### Introduction

Local reduction is an important algorithm in relation to Physics applications. In many cases, we would like to find the best match between particles in a set of events for example, or select the best result per event with respect to a given selection criteria, quickly. The selection criteria can be from simple `max()`, `min()` operation to any set of complex criteria. We want the best match *per event* that satisfies the criteria. This is what local reduction helps in.

Reduction is an inheretly sequential algorithm, which makes it very hard to vectorize properly. The fact that the **local** reduction is performed per event or segment-wise, makes it all the more complex, because data isn't necessarily localized in fixed length segements. Under such a situation, it becomes essential to make the algorithm as much vectorized/parallelized as possible. 

This is what we will explore in this report. 

Also, please note that we shall be using `max()` as the selection criteria for explaining the algorithm, but it will work for any associative operator, like `min()` or a custom function.


### Sequential Implementation

Let us consider a linear data array `arr` which contains the data for all the segments. The segments/events are separated with the `starts` and `stops` array, which stores the starting index and stopping index for the arrays in `arr`. 

Let us define an array `maxarr`, which will store the results of the `max()` operations. The sequential algorithm which dictates how it will work goes as follows:

```python

for i in range(len(starts)):
    for j in range(starts[i]:stops[i])
        maxarr[i] = max(arr[j], maxarr[i])
```

For example, if the `arr`, `starts` and `stops` are defined as 

```python

arr = [1,2,3,2,5,1,4]
starts = [0,2,3]
stops = [2,3,6]
```

Then the array segments are 

```python

arr1 = [1,2,3]
arr2 = [2]
arr3 = [5,1,4]
```

Then the local reduction with `max()`operation will result in the maxarr as

```python
maxarr = [3,2,5]
```

Let's see how to vectorize it now. Also, please keep in mind that given the extremely sequential nature of the algorithm, it may not be possible to get a lot of performance benefits from vectorizing this problem, especially for smallish data.

### Vectorized Implementation

The earliest works on vectorizing a similar algorithm,  which goes by the name of parallel-scan, or cumulative sum, or prefix sum, comes from the work of two researchers Daniel Hillis and Guy L Steele. Their work was refined and popularized by Guy E Blelloch who created the classical parallel version of the solution, known as Blelloch scan. The algorithm works on two phases, on first phase,called "up-sweep" phase, it involves decomposition of the array into multiple offsets, like a tree like structure, and decomposing it to a single value.  In the next stage, called the "down-sweep" stage, the prefix sum is estimated by adding the decomposed value to the array results in offsets. 

We draw our inspiration from their work. There are some similarities between prefix-sum and local reduction. The prefix sum is same as local reduction, if:

- There is only one segment.

- The reduction operator is `+`.

The up-sweep phase  of  the algorithm can be described by the pseudocode:

```python
for d=0 to (log2(n)-1)
    forall k=0 to 2**(d+1) do
        arr[k+2^(d+1) -1] = arr[k+2^(d)-1] + arr[k+2^(d+1)-1]
```

The algorithm proceeds as follows:

1. At first, the adjacent values are summed and stored in $`2^{nd}`$ variable.

2. The offset now changes to 2,corresponding to d=1, which now sums the elements off by 1, and stores it in 2nd value.

3. The offset now becomes 4, and so on, until the end of the array is reached, that is the offset becomes n/2.

The final value in the array is the sum of all the values in the array.

How does it work for reduction? Well,  we just need to change the `+` operator to the reduction operator, say `max()`. The last step in the algorithm will now become:

```python
arr[k+2^(d+1) -1] = max(arr[k+2**(d)-1] , arr[k+2**(d+1)-1])
```

*How to solve the issue of per-event reduction?*

The answer to it lies in the concept of events based reduction. We cannot use the classic approach in this case, as the elements between events cannot be mixed up. 

We can achieve the same using the `parents` array. The generation of this parents array has been described earlier in [parents](). The `parents` links every element to it's corresponding event. 

- [ ] Add link to parents

Using parents, we can now check for event boundaries for every element,, and hence, avoid the mixup between elements of different events.

The pseudocode describing the same is:

```python
for d=0 to (log2(n)-1)
    forall k=0 to 2**(d+1) do
        if parents[k+2^(d+1) -1] == parents[k+2^(d)-1]:
            arr[k+2^(d+1) -1] = max(arr[k+2^(d)-1] ,arr[k+2^(d+1)-1])
```

Another approach can be to vectorize across events, and find the reduction via a loop. However, it has the serious disadvantage that if one of the segments is disproportionately long, the other threads will hav to wait for it to complete, leading to slow speed. So, this has not been done.

#### GPU implementation

A simple GPU implementation of the algorithm in CUDA is shown below:

```cpp
__global__ void local_max(int* arr,int* parents, int* num_particles)
{
    unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;

    // Check for end of array
    if (tid > num_particles[0])
        return;
    
    // The loop giving offsets
    for (int d=1; d<num_particles[0]; d*=2)
    {
        if (tid >=d && parents[tid]==parents[tid-d])
        {
            arr[tid] = max(arr[tid], arr[tid-d]);
        }
        __syncthreads();
    }
}

```

Here, `num_particles` is the total number of elements in all events combined.

Note that we may have warp divergence in the branching, but it is easy to avoid it by using a strided access pattern.

#### CPU implementation

The CPU version was implemented  using numpy `maximum.reduceat()`. 


### Performance 

#### Execution environment

The codes were executed on an Amazon AWS G2 instance, which had the following specifications:

__*CPU*__
The CPU in the instance is Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz. It has 2 physical cores, and 4 logical cores, and supports SIMD instructions like AVX and SSE.

__*RAM:*__

The instance had a RAM size of 64 GB.
__*GPU:*__ 

The GPU instance was loaded with a Nvidia Tesla K80 card, which is of model number GK210GL.  It's specifications can be checked [here](https://www.nvidia.com/en-us/data-center/tesla-k80/). 

#### Timings

For timing the results, the time taken for GPU is derived using PyCUDA's `driver.Event()` module. It waits until the device has synchronized, which indicates the end of the kernel call. 

The CPU code was timed with OpenMP's `omp_get_wtime()` for C++ code, and `timeit.timeit()` for numpy version. The best of the two was kept as CPU time taken.

The relative timings were plotted with `matplotlib`. It is given below:

![Timings plot](images/max_time.png)

The plots are between wall clock time taken in milliseconds, and the Average number of particles.

It can be seen that The CPU time intercept is much higher than GPU, which means that as the data size gets larger, GPU will outperform CPU almost always.

