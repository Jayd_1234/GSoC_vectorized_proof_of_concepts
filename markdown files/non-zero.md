## Non zero elements estimation
__*CODE:*__ Available as a jupyter notebook [here](https://gitlab.com/Jayd_1234/GSoC_vectorized_proof_of_concepts/blob/master/misc/Attempts/non_zero/nonzero_copy.ipynb)

### introduction

`Non-zero` is another example of reduction in a data array. Here, we attempt to find the index of non-zero elements in the array. It is hard to vectorize easily, as we don't know the number of non-zero elements in the array from the beginning, nor is there an easy way to relate the index of non-zero element to the index in the `nonzero` array where the index should be inserted. This makes it sequential, and thus, the CPU can do well. 

However, can the GPU beat CPU through parallel operations? 

This is what we will explore now. 

### Sequential implementation

It is a classical sequential algorithm. The sequential version can easily be implemented as a two level algorithm:

- Count the total number of non-zero elements.

- Insert the non-zero elements into the array.

Let us consider that the data array is `arr`, and we would like to find the total number of non-zero elements in the array. The sequential version implementing the same will be:

```python

import numpy

# Counting block. Counts the total number of non-zero elements, 
# and stores it in count
count = 0
for i in range(0, len(arr)):
    if arr[i] != 0:
        count += 1

# Allocate the array tto store the non-zero element indices
nonzero = numpy.array(count)

# Add the values to the array
temp = 0
for i in range(0, len(arr)):
    if arr[i] != 0:
        nonzero[temp] = i
        temp += 1
    
``` 

The major barrier to the vectorization of the algorithm is the two dependent loops, the counting loops, which depends on `count`, and the array assignment loop, which depends on `temp`.

We shall discuss how to achieve vectorization in the next section.

### Vectorized implementation 

For the vectorized implementation, we will focus entirely on CUDA, i.e. GPUs. This is because efficient routines exist for `nonzero` in scientific libraries like `numpy`, and moreover, the compiler can usually vectorize the loop through AVX instructions. This is however, quite different on GPUs, which follow a Single Instruction Multiple Threads (SIMT) programming model. It is quite essential to explore alternate programming structure for the same to take advantage of this model. 

This will be discussed next.

#### GPU Implementation

For the GPU Implementation, we draw inspiration from the "InK compact" algorithm developed by Hughes et al. It is available [here](https://onlinelibrary.wiley.com/doi/abs/10.1111/cgf.12083).

The algorithm is structured in the following way:

1. The data is divided into CUDA thread blocks, and the number of non-zero elements in each thread block is calculated. 

2. An exclusive prefix sum is evaluated on the result of the counts. This will give us the starting offset for the non-zero array counting in the next block.

3. The number of non-zero elements for each warp in the thread block is counted, and for each thread, the corresponding thread index addded to the offset is stored in the non-zero array.

Each step will be explained next.

##### Counting non-zero elements in a thread block

In this step, we calculate the total number of non-zero elements in a block of threads. This can be done in two ways: 

- Running a loop over all the elements in the block, and count. 

- Using the CUDA intrinsic `__syncthreads_count()`. 

It was shown by Hughes eta al that the second approach performs much better, as it has been tuned to Nvidia GPUs to achieve optimal performance.

The routine proceeds as follows: First a predicate function is established, which return a true value, if the condition is met, else false. In our case, the predicate is `arr[i] != 0`. Next, the predicate is evaluated for all threads in the block. Finally, the total number of threads returning true is returned as the count of non-zero elements.

The code doing the same is described below:

```cpp

// Predicate function
__device__ int predicate(float x)
{
    return (x !=0)?1:0;
}

// Counts per block
__global__ void blockCounts(float *data, int* blockcounts,int* length)
{
    unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;
    
    if (tid >= length[0])
        return;
    
    // Evaluate predicate for every thread
    int validity = predicate(data[tid]);

    // Count the number of true threads.
    int blockcount = __syncthreads_count(validity);

    // Add the counts for the block to it's block id in blockcounts
    if ( threadIdx.x == 0)
        blockcounts[blockIdx.x] = blockcount;
}
```

Here, the `blockcounts` array stores the count of the blocks, that evaluate threads to true.

##### Determining block offsets from block counts

To determine the offset or the starting index of the non-zero element to be processed by the next thread block. 

This can be evaluated by running an exclusive prefix sum on the `blockcounts`. Since this is already implemented in an efficient manner in PyCUDA, we use that optimized routine. 

The CUDA function doing the same is given below:

```python
from pycuda.scan import ExclusiveScanKernel

# Define the scan kernel
scan_kern = ExclusiveScanKernel(np.int, 'a+b', neutral=0)

# Evaluate the kernel
scan_kern(gpu_blockcounts,gpu_blockoffsets)
```

Note that here, the `gpu_blockcounts` is the GPUArray instance of `blockcounts`.  SImilarly, the `blockoffsets` is represented by the GPUArray instance `gpu_blockoffsets`.

##### Inserting the non-zero element indexes to it's position in the storage array

This is the final step of the compaction procedure. The implementation of the code has been modeled around the work of [David Spataro](http://www.davidespataro.it/). It has been modified and slightly optimized to work on the problem.

It works as follows:

- A `warpres` shared memory variable is initialized, which stores the result of compaction for each warp in the block.

- The predicate is evaluated for each thread in the warp.

- 32-bit mask with bits set to 1 in all positions less than the thread's lane number in the warp.

- The number of active ( i.e. predicate evaluating to 1) threads is returned as a 32 bit integer by `__ballot()`, whose nth bit is set if and only if the the thread represented by that bit is active. 

- By masking the result of __ballot() such that bits representing threads after the current thread are set to zero, and counting the set-bits, we get the number of threads before the current thread (in the warp) that will write an output.` __popc(int v)` intrinsic returns the number of bits set in the binary representation of integer v.
 
 - The total number of writes within warp can be found by the number of threads writing before the last thread within warp plus 1, depending on the value of the predicate corresponding to the last thread within warp. The total number of writes per warp is written by the last thread within the warp.
 
-  The next stage is to find the total number of writes before the current warp which  serves as the output offset for the warp. This is performed by a scan operation on all the number of threads writing per warp. 

- Finally, the result is written to the output array, if the predicate evaluates true.


The full code has been shown below.
```cpp
__global__ void compact(float* data,int* output,int* blockoffsets,int* length)
{
    unsigned int tid = threadIdx.x + blockIdx.x*blockDim.x;
    __shared__ int warpres[32];
    int warpSize = 32;
    if ( tid < length[0])
    {
        int pred = predicate(data[tid]);
        int w_i = threadIdx.x/warpSize; 
        int w_l = tid % warpSize;
        int t_m = INT_MAX >> (warpSize-w_l-1); 

        int b	= __ballot(pred) & t_m; 
        int t_u	= __popc(b);
        
        if(w_l==warpSize-1){
            warpres[w_i]=t_u+pred;
        }
        __syncthreads();


        if(w_i==0 && w_l<blockDim.x/warpSize){
            int w_i_u=0;
            for(int j=0;j<=5;j++){
                int b_j =__ballot( warpres[w_l] & (1<<j) );
                w_i_u += (__popc(b_j & t_m)  ) << j;

            }
            warpres[w_l]=w_i_u;
        }

        __syncthreads();


        if(pred){
            output[t_u+warpres[w_i]+blockoffsets[blockIdx.x]]= tid;

        }

    }
}
```



### Performance 

#### Execution environment

The codes were executed on an Amazon AWS G2 instance, which had the following specifications:

__*CPU*__
The CPU in the instance is Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz. It has 2 physical cores, and 4 logical cores, and supports SIMD instructions like AVX and SSE.

__*RAM:*__

The instance had a RAM size of 64 GB.
__*GPU:*__ 

The GPU instance was loaded with a Nvidia Tesla K80 card, which is of model number GK210GL.  It's specifications can be checked [here](https://www.nvidia.com/en-us/data-center/tesla-k80/). 

#### Timings

For timing the results, the time taken for GPU is derived using PyCUDA's `driver.Event()` module. It waits until the device has synchronized, which indicates the end of the kernel call. 

The CPU code was timed with OpenMP's `omp_get_wtime()` for C++ code, and `timeit.timeit()` for numpy version. The best of the two was kept as CPU time taken.

The relative timings were plotted with `matplotlib`. It is given below:

![Timings plot](images/nonzero_time.png)

The plots are between wall clock time taken in milliseconds, and the Average number of particles.

It can be seen that The CPU time intercept is much higher than GPU, which means that as the data size gets larger, GPU will outperform CPU almost always.