## Offsets from parents

__*CODE:*__ Available [here](https://gitlab.com/Jayd_1234/GSoC_vectorized_proof_of_concepts/blob/master/non-rejection-mode/offsets_from_parents.py)
### Introduction

Offsets from parents ( hereinafter called `offsets`) calculates the offset/starting-index of each event given the `parents` array for the entire array. The `parents` array relates each element to it's corresponding event in the array `arr`.  


### Sequential Implementation

The sequential implementation of the function is easy to get, given the `parents` array, and the total length of the array `arr`.

One such implementation in python is described below:

```python
count = 0
for i in range(length(arr)-1):
    if (parents[i] != parents[i+1])
        offsets[count] = i+1
        count += 1
```

Note that the `offsets` array pucks up the positions, where `parents` change, with the assumption that `parents` is arranged in an increasing contiguous fashion. Since the index where the `offset` must be updated depends on `counts` variable, this makes the problem non-trivially vectorizable.

### Vectorized Implementation

For deriving the vectorized solution, we can decompose the problem into two major parts:

- Find out the positions where `parents[i] != parents[i+1]`.

- Find out the total number of such cases.

- Stream compaction to remove the unnecessary values from the `offsets` array.

In the special case of offsets, the steps can be merged within a single kernel call, because we can get the index for the offsets from `parents` array. The process can be explained as per the following steps:

1. Find out the number of elements to be included in `offsets` array. For this we need the maximum value in parents array. Note that this step is just for data initialization, and not part of main algorithm, and so not included in timings.

    ```python
    import numpy

    max_val = parents.max()
    offsets = numpy.zeros(max_val+2, dtype=numpy.int)
    ```

2. Run `len(parents)` number of threads ( indexed by `i` say), and find the index `i` where `parents[i] != parents[i+1]`.

3. Update `offsets[parents[i+1]] = i+1`.

Note that we could do the update to `offsets` easily, because we have the value for offsets.

##### GPU Implementation

The GPU implementation can be done on **_CUDA_** equivalently. The main kernel is described below:

```cpp
__global__ void offsets_from_parents(int* parents,int* offsets, int* num_parents)
{
    // Thread IDs
    unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;

    // Check for corner case, make sure we don't exceed the boundary.
    if (idx >= num_parents[0] - 1)
        return;
    
    // Store the values necessary as local varsiables to reduce data R/W
    int prev_val = parents[idx];
    int next_val = parents[idx + 1];

    // Check for the condition and write the results
    if (prev_val != next_val)
    {
        offsets[next_val] = idx+1;
    }
}
```

Here, `num_parents` is a single variable array, that gives the number of elements in `parents`. Note that we consider `numparents[0] - 1` threads, because we are checking by an offset of 1.

##### CPU Implementation

The CPU code was implemented as numpy code. It is based on almost the same logic.`



### Performance 

#### Execution environment

The codes were executed on an Amazon AWS G2 instance, which had the following specifications:

__*CPU*__
The CPU in the instance is Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz. It has 2 physical cores, and 4 logical cores, and supports SIMD instructions like AVX and SSE.

__*RAM:*__

The instance had a RAM size of 64 GB.
__*GPU:*__ 

The GPU instance was loaded with a Nvidia Tesla K80 card, which is of model number GK210GL.  It's specifications can be checked [here](https://www.nvidia.com/en-us/data-center/tesla-k80/). 

#### Timings

For timing the results, the time taken for GPU is derived using PyCUDA's `driver.Event()` module. It waits until the device has synchronized, which indicates the end of the kernel call. 

The CPU code was timed with OpenMP's `omp_get_wtime()` for C++ code, and `timeit.timeit()` for numpy version. The best of the two was kept as CPU time taken.

The relative timings were plotted with `matplotlib`. It is given below:

![Timings plot](images/offsets_time.png)

The plots are between wall clock time taken in milliseconds, and the Average number of particles.

It can be seen that The CPU time intercept is much higher than GPU, which means that as the data size gets larger, GPU will outperform CPU almost always.